package domain.use_case

import domain.entities.RegionAlertInfo
import domain.repositories.AlertsRepository
import domain.use_case.base.FlowUnitUseCase
import kotlinx.coroutines.flow.Flow

class GetActiveAlertsUseCase(private val alertsRepository: AlertsRepository) :
    FlowUnitUseCase<List<RegionAlertInfo>>() {
    override fun execute(): Flow<Result<List<RegionAlertInfo>>> = alertsRepository.getActiveAlerts()
}