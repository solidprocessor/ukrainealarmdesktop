// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import data.DataModules
import device.sound.DeviceModules
import domain.DomainModules
import domain.entities.SelectedRegion
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.core.Koin
import org.koin.core.context.GlobalContext.startKoin
import presentation.common.controller.ControllerState
import presentation.common.navigation.Navigation
import presentation.common.navigation.NavigationDirection
import presentation.common.navigation.NavigationProvider
import presentation.core.PresentationModules
import presentation.ui.constants.Colors
import presentation.ui.regions.SavedRegionController

private fun initKoin(): Koin =
    startKoin {
        modules(
            DataModules.ALL
                    + DomainModules.ALL
                    + DeviceModules.ALL
                    + PresentationModules.ALL
        )
    }.koin

private fun initNavigation(koin: Koin) =
    NavigationProvider(koin.get(), koin.get())

private fun initSavedRegionController(koin: Koin) =
    SavedRegionController(koin.get(), koin.get())

fun main() = application {
    val koin = initKoin()
    val navigationProvider = initNavigation(koin)
    val savedRegionController = initSavedRegionController(koin)
    GlobalScope.launch {
        savedRegionController.refresh()
        savedRegionController.state.collect {
            performInitialNavigation(it)
        }
    }
    Window(onCloseRequest = ::exitApplication, title = "Тривога!", icon = painterResource("ic_launcher.png")) {
        Box(modifier = Modifier.background(Colors.BACKGROUND_GRAY)) {
            val currentScreen by Navigation.currentScreen.collectAsState()
            navigationProvider.navigate(currentScreen)
        }
    }
}

private fun performInitialNavigation(state: ControllerState<SelectedRegion?>) {
    when (state) {
        is ControllerState.Data -> {
            Navigation.currentScreen.value =
                state.data?.let(NavigationDirection::AlertScreen) ?: NavigationDirection.RegionsScreen()
        }
        is ControllerState.Error -> {
            Navigation.currentScreen.value = NavigationDirection.RegionsScreen()
        }
        is ControllerState.Loading -> Unit
    }
}
