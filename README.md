# Ukraine Alert
## _Desktop application for notifying about the alerts in Ukraine_

[![UkraineAlarm](https://static.wixstatic.com/media/1420a6_5dcdcf9b9fad4d8495119b6f47c92bdc~mv2.png/v1/fill/w_108,h_110,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/Mask%20group.png)](https://www.ukrainealarm.com/)
Based on [UkraineAlarm](https://www.ukrainealarm.com/)

Written  with [Compose Multiplatform](https://www.jetbrains.com/lp/compose-mpp/)

## Features
- Selecting a ukranian region from the provided list
- Observing the region status with a periodic request (every 15 seconds)
- Sound notifications about the declared alert / alert cancel
- Manual alert notification
