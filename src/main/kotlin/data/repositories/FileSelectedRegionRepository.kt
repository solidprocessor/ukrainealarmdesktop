package data.repositories

import domain.entities.SelectedRegion
import domain.repositories.SelectedRegionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import java.io.*


object FileSelectedRegionRepository : SelectedRegionRepository {

    private const val STORAGE_FILE_NAME = "ukraineAlertSavedRegion"

    override suspend fun save(region: SelectedRegion): Result<String> =
        runBlocking(Dispatchers.IO) {
            try {
                val storage = getStorageFile()
                val outputStream = FileOutputStream(storage)
                val objectOutputStream = ObjectOutputStream(outputStream)
                objectOutputStream.writeObject(region)
                Result.success(storage.path)
            } catch (error: Exception) {
                Result.failure(error)
            }
        }

    override suspend fun get(): Result<SelectedRegion?> =
        runBlocking(Dispatchers.IO) {
            try {
                val storage = getStorageFile()
                Result.success(
                    if (storage.exists()) {
                        val inputStream = FileInputStream(storage)
                        val objectInputStream = ObjectInputStream(inputStream)
                        objectInputStream.readObject() as SelectedRegion
                    } else {
                        null
                    }
                )
            } catch (error: Exception) {
                Result.failure(error)
            }
        }

    private fun getCurrentFolder() = File(this::class.java.protectionDomain.codeSource.location.toURI())

    private fun getStorageFile() = getCurrentFolder().let {
        val storage = File(it, STORAGE_FILE_NAME)
        if (!storage.exists()) {
            storage.createNewFile()
        }
        println("storage file: ${storage.path}")
        storage
    }

}