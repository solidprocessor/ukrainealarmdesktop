package domain.entities

data class BasicStatusInfo(
    val regionId: String,
    val regionName: String?,
    val alertType: AlertType,
    val lastUpdate: String
) {
    fun isAlertActive() =
        alertType == AlertType.AIR || alertType == AlertType.STREET_FIGHT
}