package data.mappers

import data.models.RetrofitStatusObject
import domain.entities.AlertType
import domain.entities.BasicStatusInfo

object BasicStatusMapper : OneDirectionMapper<RetrofitStatusObject, BasicStatusInfo?> {
    override fun map(model: RetrofitStatusObject): BasicStatusInfo? =
        model.run {
            if (regionId != null && lastUpdate != null) {
                val alertTypeString = activeAlerts?.firstOrNull { it.regionId == regionId }?.type
                val alertType = alertTypeString?.let(AlertTypeParser::parse) ?: AlertType.NONE
                BasicStatusInfo(
                    regionId = regionId,
                    regionName = regionName,
                    alertType = alertType,
                    lastUpdate = lastUpdate
                )
            } else {
                print("some required value is null: $this")
                null
            }
        }
}