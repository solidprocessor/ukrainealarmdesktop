package presentation.common.navigation

import kotlinx.coroutines.flow.MutableStateFlow

object Navigation {
    val currentScreen = MutableStateFlow<NavigationDirection>(NavigationDirection.Pending)
}