package data.test

import domain.entities.AlertType
import domain.entities.BasicStatusInfo
import domain.entities.RegionAlertInfo

object TestRegionAlerts {
    val NoAlert = RegionAlertInfo.State(
        BasicStatusInfo(
            regionId = "testRegion",
            regionName = "Тестова область",
            alertType = AlertType.NONE,
            ""
        )
    )
    val Alert = RegionAlertInfo.State(
        BasicStatusInfo(
            regionId = "testRegion",
            regionName = "Тестова область",
            alertType = AlertType.AIR,
            ""
        )
    )
}