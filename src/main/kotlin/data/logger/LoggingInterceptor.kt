package data.logger

import okhttp3.logging.HttpLoggingInterceptor

object LoggingInterceptor {
    fun build(
        logger: HttpLoggingInterceptor.Logger = PrintLogger,
        level: HttpLoggingInterceptor.Level = HttpLoggingInterceptor.Level.BODY,
        headerToRedact: String? = null
    ) =
        HttpLoggingInterceptor(logger).apply {
            this.level = level
            if (headerToRedact != null) {
                redactHeader(headerToRedact)
            }
        }
}