package data.mappers

import data.models.RetrofitCommunity
import data.models.RetrofitDistrict
import data.models.RetrofitState
import domain.entities.AlertType
import domain.entities.BasicStatusInfo
import domain.entities.RegionAlertInfo

object RegionsMapper : OneDirectionMapper<List<RetrofitState>, List<RegionAlertInfo.State>> {
    override fun map(model: List<RetrofitState>): List<RegionAlertInfo.State> =
        model.mapNotNull {
            it.toEntity()
        }

    private fun RetrofitState.toEntity(): RegionAlertInfo.State? =
        toBasicInfo()?.let { basicStatusInfo ->
            RegionAlertInfo.State(basicStatusInfo,
                districts = ArrayList(districts?.mapNotNull { it.toEntity() } ?: emptyList())
            )
        } ?: run {
            print("cannot parse the state: $this")
            null
        }

    private fun RetrofitDistrict.toEntity(): RegionAlertInfo.District? =
        toBasicInfo()?.let { basicStatusInfo ->
            RegionAlertInfo.District(basicStatusInfo,
                communities = ArrayList(communities?.mapNotNull { it.toEntity() } ?: emptyList())
            )
        } ?: run {
            print("cannot parse the district: $this")
            null
        }

    private fun RetrofitCommunity.toEntity(): RegionAlertInfo.Community? =
        toBasicInfo()?.let(RegionAlertInfo::Community)
            ?: run {
                print("cannot parse the community: $this")
                null
            }

    private fun RetrofitState.toBasicInfo(): BasicStatusInfo? = regionId?.let {
        BasicStatusInfo(
            regionId = regionId, regionName = regionName, AlertType.NONE, lastUpdate = ""
        )
    }

    private fun RetrofitDistrict.toBasicInfo(): BasicStatusInfo? = regionId?.let {
        BasicStatusInfo(
            regionId = regionId, regionName = regionName, AlertType.NONE, lastUpdate = ""
        )
    }

    private fun RetrofitCommunity.toBasicInfo(): BasicStatusInfo? = regionId?.let {
        BasicStatusInfo(
            regionId = regionId, regionName = regionName, AlertType.NONE, lastUpdate = ""
        )
    }
}