package presentation.ui.regions

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import domain.entities.RegionAlertInfo
import presentation.common.composables.ErrorComposables
import presentation.common.composables.Modifiers
import presentation.common.composables.Search
import presentation.common.composables.Texts
import presentation.common.controller.ControllerState
import presentation.common.extensions.getName
import presentation.common.navigation.Navigation
import presentation.common.navigation.NavigationDirection
import presentation.common.themes.Dark
import presentation.ui.constants.Colors
import presentation.ui.constants.Fonts
import presentation.ui.constants.Paddings.REGION_PADDING

class RegionsComposable(
    private val regionsController: RegionsController,
    private val savedRegionController: SavedRegionController,
) {

    @Composable
    fun compose(selectedRegion: RegionAlertInfo? = null) {
        val state by regionsController.state.collectAsState()
        regionsController.refresh()
        MaterialTheme(colors = Dark.colors()) {
            Column(
                modifier = Modifiers.fill().padding(16.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                when (state) {
                    is ControllerState.Data -> {
                        val dataState = state as ControllerState.Data
                        regions(dataState.data)
                    }
                    is ControllerState.Error -> {
                        val error = (state as ControllerState.Error).error
                        ErrorComposables.error(
                            title = "Вибачте, виникла помилка при завантаженні регіонів",
                            message = error.toString()
                        )
                    }
                    is ControllerState.Loading -> {
                        CircularProgressIndicator(color = Colors.YELLOW)
                    }
                }
            }
        }
    }

    @Composable
    private fun regions(states: List<RegionAlertInfo.State>) {
        val scrollState = rememberScrollState()
        val searchText = remember { mutableStateOf<String?>(null) }
        val selectedRegion = remember { mutableStateOf<RegionAlertInfo?>(null) }
        Column {
            Row {
                Search.searchWithButton {
                    searchText.value = it
                }
            }
            Divider(color = Color.White, modifier = Modifier.padding(vertical = 16.dp))
            Row {
                val source = searchText.value?.let {
                    RegionsFilter.filter(it, states)
                } ?: states
                Column(
                    modifier = Modifiers.fill().verticalScroll(scrollState),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    source.map {
                        Row { StateRegion(it, searchText.value != null) { selectedRegion.value = it } }
                    }
                    selectedRegion.value?.let(::goToAlertScreen)
                }
            }
        }
    }

    @Composable
    private fun StateRegion(
        region: RegionAlertInfo.State,
        isFiltering: Boolean = false,
        regionSelectionListener: (RegionAlertInfo) -> Unit,
    ) {
        val collapsed = remember { mutableStateOf(true) }
        Column(
            Modifier.fillMaxWidth().padding(bottom = 16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row {
                Texts.clickableText(
                    text = region.getName("Невідома область"),
                    textSize = Fonts.Sizes.STATE,
                    paddingBottom = 16.dp
                ) {
                    collapsed.value = !collapsed.value
                }
            }
            if (isFiltering || !collapsed.value) {
                Row {
                    Texts.clickableText(
                        text = "Обрати всю область",
                        textSize = Fonts.Sizes.STATE,
                        textColor = Colors.SELECTABLE_REGION_TEXT_COLOR,
                        decoration = TextDecoration.Underline,
                        paddingBottom = 16.dp
                    ) {
                        regionSelectionListener(region)
                    }
                }
                region.districts.map {
                    Row(Modifier.padding(start = REGION_PADDING)) {
                        DistrictRegion(it, isFiltering, regionSelectionListener)
                    }
                }
            }
        }
    }

    @Composable
    private fun DistrictRegion(
        region: RegionAlertInfo.District,
        isFiltering: Boolean = false,
        regionSelectionListener: (RegionAlertInfo) -> Unit,
    ) {
        val collapsed = remember { mutableStateOf(true) }
        Column(
            Modifier.fillMaxWidth().padding(bottom = 16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row {
                Texts.clickableText(
                    text = region.getName("Невідомий район"),
                    textSize = Fonts.Sizes.DISTRICT,
                    paddingBottom = 16.dp
                ) {
                    collapsed.value = !collapsed.value
                }
            }
            if (isFiltering || !collapsed.value) {
                Row {
                    Texts.clickableText(
                        text = "Обрати весь район",
                        textSize = Fonts.Sizes.DISTRICT,
                        textColor = Colors.SELECTABLE_REGION_TEXT_COLOR,
                        decoration = TextDecoration.Underline,
                        paddingBottom = 16.dp
                    ) {
                        regionSelectionListener(region)
                    }
                }
                region.communities.map {
                    Row(Modifier.padding(start = REGION_PADDING)) { CommunityRegion(it, regionSelectionListener) }
                }
            }
        }
    }

    @Composable
    private fun CommunityRegion(
        region: RegionAlertInfo.Community,
        regionSelectionListener: (RegionAlertInfo) -> Unit,
    ) {
        Texts.clickableText(
            text = region.getName("Невідома територіальна громада"),
            textSize = Fonts.Sizes.COMMUNITY,
            textColor = Colors.SELECTABLE_REGION_TEXT_COLOR,
            decoration = TextDecoration.Underline,
            paddingBottom = 16.dp
        ) {
            regionSelectionListener(region)
        }
    }

    private fun goToAlertScreen(selectedRegion: RegionAlertInfo) {
        val region = savedRegionController.save(selectedRegion)
        regionsController.cancel()
        Navigation.currentScreen.value = NavigationDirection.AlertScreen(region)
    }
}