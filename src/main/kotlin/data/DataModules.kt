package data

import com.ukraine.UkraineAlertCompose.BuildConfig
import data.repositories.FileSelectedRegionRepository
import data.repositories.RetrofitAlertsRepository
import data.retrofit.AlertsApi
import data.retrofit.OkhttpClientBuilder
import data.retrofit.RetrofitBuilder
import data.test.TestAlertsRepository
import domain.repositories.AlertsRepository
import domain.repositories.SelectedRegionRepository
import org.koin.dsl.module
import retrofit2.Retrofit

@Suppress("MemberVisibilityCanBePrivate")
object DataModules {

    val RETROFIT = module {
        single { OkhttpClientBuilder.build() }
        single { RetrofitBuilder.build(get()) }
        single<AlertsApi> { get<Retrofit>().create(AlertsApi::class.java) }
    }

    //Requires retrofit instance
    val REPOSITORY = module {
        single<AlertsRepository> {
            if (!BuildConfig.TEST_MODE) {
                RetrofitAlertsRepository(get())
            } else {
                TestAlertsRepository
            }

        }
        single<SelectedRegionRepository> { FileSelectedRegionRepository }
        //single<AlertsRepository> { TestAlertsRepository }
    }

    val ALL = RETROFIT + REPOSITORY
}