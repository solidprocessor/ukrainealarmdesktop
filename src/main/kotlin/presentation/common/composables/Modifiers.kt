package presentation.common.composables

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import presentation.ui.constants.Colors

object Modifiers {
    fun fill() =
        Modifier.fillMaxWidth().fillMaxHeight()

    fun fillWithPadding() =
        fill().padding(16.dp)

    fun fillWithBorder(color: Color = Colors.BACKGROUND_GRAY) =
        fill().border(16.dp, color).padding(16.dp)
}