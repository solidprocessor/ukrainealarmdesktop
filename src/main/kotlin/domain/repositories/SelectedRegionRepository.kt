package domain.repositories

import domain.entities.SelectedRegion

interface SelectedRegionRepository {
    suspend fun save(region: SelectedRegion): Result<String>
    suspend fun get(): Result<SelectedRegion?>
}