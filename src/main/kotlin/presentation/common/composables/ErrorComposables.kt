package presentation.common.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.sp
import presentation.ui.constants.Colors

object ErrorComposables {
    @Composable
    fun error(title: String, message: String) {
        Column(Modifiers.fillWithBorder(Colors.ERROR_BACKGROUND).background(Colors.ERROR_BACKGROUND)) {
            Row {
                Text(title,
                    color = Color.White,
                    fontSize = 24.sp
                )
            }
            Divider(color = Color.White)
            Row {
                Text(message,
                    color = Color.White,
                    fontSize = 18.sp
                )
            }
        }
    }
}