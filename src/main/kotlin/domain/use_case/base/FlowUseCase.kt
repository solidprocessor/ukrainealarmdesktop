package domain.use_case.base

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

abstract class FlowUseCase<Params, Data> {
    open operator fun invoke(params: Params) = execute(params).flowOn(Dispatchers.IO)

    protected abstract fun execute(params: Params): Flow<Result<Data>>
}