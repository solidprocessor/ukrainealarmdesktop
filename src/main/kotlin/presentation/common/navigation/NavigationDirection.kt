package presentation.common.navigation

import domain.entities.RegionAlertInfo
import domain.entities.SelectedRegion

sealed interface NavigationDirection {
    object Pending : NavigationDirection
    class RegionsScreen(val selectedRegion: RegionAlertInfo? = null) : NavigationDirection
    class AlertScreen(val selectedRegion: SelectedRegion) : NavigationDirection
}