package presentation.common.controller

import domain.entities.errors.AppError

sealed interface ControllerState<T> {
    class Loading<T> : ControllerState<T>
    class Error<T>(val error: AppError) : ControllerState<T>
    class Data<T>(val data: T) : ControllerState<T>
}