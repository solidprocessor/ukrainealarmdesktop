package domain.use_case

import domain.entities.RegionAlertInfo
import domain.repositories.AlertsRepository
import domain.use_case.base.FlowUnitUseCase
import kotlinx.coroutines.flow.Flow

class GetRegionsUseCase(private val alertsRepository: AlertsRepository) :
    FlowUnitUseCase<List<RegionAlertInfo.State>>() {

    override fun execute(): Flow<Result<List<RegionAlertInfo.State>>> = alertsRepository.getRegions()
}