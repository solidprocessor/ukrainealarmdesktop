package data.test

import domain.entities.RegionAlertInfo
import domain.repositories.AlertsRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.shareIn

object TestAlertsRepository : AlertsRepository {
    override fun getRegions(): Flow<Result<List<RegionAlertInfo.State>>> = flow {
        emit(Result.success(listOf(TestRegionAlerts.NoAlert)))
    }

    private val testStatusFlow = flow {
        emit(Result.success(TestRegionAlerts.NoAlert))
        delay(10000)
        emit(Result.success(TestRegionAlerts.Alert))
        delay(2 * 60 * 1000)
        emit(Result.success(TestRegionAlerts.NoAlert))
    }.shareIn(GlobalScope, SharingStarted.Lazily)

    override fun getStatusOfRegion(regionId: String): Flow<Result<RegionAlertInfo>> = testStatusFlow

    override fun getActiveAlerts(): Flow<Result<List<RegionAlertInfo>>> {
        TODO("Not yet implemented")
    }
}