package data.repositories

import data.mappers.RegionStatusMapper
import data.mappers.RegionsMapper
import data.retrofit.AlertsApi
import domain.entities.RegionAlertInfo
import domain.repositories.AlertsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class RetrofitAlertsRepository(private val api: AlertsApi) : AlertsRepository {

    override fun getRegions(): Flow<Result<List<RegionAlertInfo.State>>> =
        flow {
            emit(
                try {
                    println("requesting full status")
                    Result.success(RegionsMapper.map(api.getRegions().states))
                } catch (exception: Exception) {
                    println("get full status error")
                    exception.printStackTrace()
                    Result.failure(exception)
                }
            )
        }

    override fun getStatusOfRegion(regionId: String): Flow<Result<RegionAlertInfo>> =
        flow {
            emit(
                try {
                    println("requesting region status")
                    val info = api.getStatusOfRegion(regionId)
                    val status = RegionStatusMapper.map(info)
                    if (status != null) {
                        Result.success(status)
                    } else {
                        Result.failure(NoSuchElementException("Cannot map region with id $regionId: $info"))
                    }
                } catch (exception: Exception) {
                    println("get region status error")
                    exception.printStackTrace()
                    Result.failure(exception)
                }
            )
        }

    @Deprecated("response model is updated")
    override fun getActiveAlerts(): Flow<Result<List<RegionAlertInfo>>> =
        flow {
            emit(
                try {
                    println("requesting active alerts")
                    // Result.success(api.getAlerts().mapNotNull(SingleAlertMapper::map))
                    Result.failure(IllegalAccessException("active alerts function is deprecated"))
                } catch (exception: Exception) {
                    println("get active alerts error")
                    exception.printStackTrace()
                    Result.failure(exception)
                }
            )
        }

}