package domain.repositories

import domain.entities.RegionAlertInfo
import kotlinx.coroutines.flow.Flow

interface AlertsRepository {
    fun getRegions(): Flow<Result<List<RegionAlertInfo.State>>>
    fun getStatusOfRegion(regionId: String): Flow<Result<RegionAlertInfo>>
    fun getActiveAlerts(): Flow<Result<List<RegionAlertInfo>>>
}