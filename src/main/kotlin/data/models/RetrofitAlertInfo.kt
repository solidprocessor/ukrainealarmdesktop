package data.models

data class RetrofitAlertInfo(
    val id: Int? = null,
    val regionId: Int? = null,
    val regionName: String? = null,
    val status: Boolean? = null,
    val lastUpdate: Long? = null,
    val parentRegionId: Int? = null,
    val regionType: String? = null,
    val alertType: String? = null,
)