package presentation.ui.alert

import device.sound.MediaPlayer
import domain.entities.RegionAlertInfo
import domain.use_case.ObserveRegionStatusUseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import presentation.common.controller.ControllerState
import presentation.common.extensions.toControllerState

class RegionStatusController(
    private val observeRegionStatusUseCase: ObserveRegionStatusUseCase,
    private val mediaPlayer: MediaPlayer,
) {
    val status: MutableStateFlow<ControllerState<RegionAlertInfo>> = MutableStateFlow(ControllerState.Loading())
    private var observingJob: Job? = null

    fun startObserving(regionId: String) {
        observingJob = GlobalScope.launch {
            observeRegionStatusUseCase(regionId).collect {
                status.emit(it.toControllerState())
            }
        }
    }

    fun stopObserving() {
        observingJob?.cancel()
        observingJob = null
    }

    fun playAlertSound() {
        mediaPlayer.playAlert()
    }

    fun playAlertCancelSound() {
        mediaPlayer.playAlertCancel()
    }

    fun stopPlaying() {
        mediaPlayer.stopPlaying()
    }
}