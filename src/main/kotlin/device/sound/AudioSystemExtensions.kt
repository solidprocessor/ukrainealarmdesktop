package device.sound

import androidx.compose.ui.res.useResource
import java.io.BufferedInputStream
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.Clip

fun Clip.loadFromResource(resourcePath: String): Clip =
    useResource(resourcePath) {
        val stream = AudioSystem.getAudioInputStream(BufferedInputStream(it))
        open(stream)
        this
    }
