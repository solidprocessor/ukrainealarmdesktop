package presentation.ui.constants

import androidx.compose.ui.unit.sp

object Fonts {
    object Sizes {
        val SEARCH = 24.sp
        val SEARCH_HINT = 18.sp

        val STATE = 24.sp
        val DISTRICT = 24.sp
        val COMMUNITY = 24.sp

        val ALERT_TITLE = 24.sp
    }
}