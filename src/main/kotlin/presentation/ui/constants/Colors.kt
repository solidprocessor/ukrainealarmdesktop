package presentation.ui.constants

import androidx.compose.ui.graphics.Color

object Colors {
    //Common:
    val TEXT_COLOR = Color.White
    val BACKGROUND_GRAY = Color(43, 43, 43)
    val BLUE = Color(0, 91, 187)
    val YELLOW = Color(255, 213, 0)
    val RED_BUTTON_BACKGROUND = Color(255, 46, 46)
    val ERROR_BACKGROUND = Color(128, 0, 0)

    //Regions:
    val SELECTABLE_REGION_TEXT_COLOR = Color(255, 247, 133)

    val TEXT_COLOR_ALERT = Color(255, 46, 46)
    val TEXT_COLOR_WARNING = YELLOW
}