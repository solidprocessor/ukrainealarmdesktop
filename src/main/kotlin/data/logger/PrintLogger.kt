package data.logger

import okhttp3.logging.HttpLoggingInterceptor

object PrintLogger : HttpLoggingInterceptor.Logger {
    override fun log(message: String) {
        println(message)
    }
}