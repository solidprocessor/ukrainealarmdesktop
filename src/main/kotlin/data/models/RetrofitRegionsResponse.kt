package data.models

import com.google.gson.annotations.SerializedName

data class RetrofitRegionsResponse(
    @SerializedName("states")
    val states: List<RetrofitState>
)