package data.mappers

interface OneDirectionMapper<From, To> {
    fun map(model: From): To
}