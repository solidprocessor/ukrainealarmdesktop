package presentation.common.extensions

import domain.entities.errors.AppError
import domain.entities.errors.CannotAccessGoogleException
import presentation.common.controller.ControllerState

fun <T> Result<T>.toControllerState() =
    when {
        isSuccess -> ControllerState.Data(getOrThrow())
        isFailure -> ControllerState.Error(mapException(exceptionOrNull()!!))
        else -> ControllerState.Loading()
    }

private fun mapException(throwable: Throwable): AppError =
    if (throwable is CannotAccessGoogleException) {
        AppError.NoInternetError
    } else {
        AppError.UnhandledError(throwable)
    }