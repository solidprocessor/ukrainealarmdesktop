package presentation.common.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.text.TextStyle
import presentation.common.extensions.toSearchText
import presentation.ui.constants.Colors
import presentation.ui.constants.Fonts

object Search {
    @OptIn(ExperimentalComposeUiApi::class)
    @Composable
    fun searchWithButton(onValueChange: (String?) -> Unit) {
        val focusRequester = remember { FocusRequester() }
        Row {
            val searchText = remember { mutableStateOf<String?>(null) }
            Column {
                TextField(
                    value = searchText.value ?: "",
                    colors = TextFieldDefaults.outlinedTextFieldColors(textColor = Colors.SELECTABLE_REGION_TEXT_COLOR),
                    label = {
                        Text("Назва регіону", fontSize = Fonts.Sizes.SEARCH_HINT)
                    },
                    modifier = Modifier.focusRequester(focusRequester).onKeyEvent {
                        if (it.key == Key.Enter) {
                            onValueChange(searchText.value)
                            true
                        } else {
                            false
                        }
                    },
                    textStyle = TextStyle(fontSize = Fonts.Sizes.SEARCH),
                    onValueChange = {
                        val searchValue = it.toSearchText()
                        searchText.value = searchValue
                        if (searchValue == null) {
                            onValueChange(null)
                        }
                    }
                )
            }
            Column {
                TextButton(
                    onClick = { onValueChange(searchText.value) },
                    Modifier.background(Color.Transparent)
                ) {
                    Text("Пошук", fontSize = Fonts.Sizes.SEARCH)
                }
            }
        }
        LaunchedEffect(Unit) {
            focusRequester.requestFocus()
        }
    }
}