package presentation.ui.regions

import domain.entities.RegionAlertInfo

//TODO improve filtering and make districts and communities immutable
object RegionsFilter {

    fun filter(text: String, states: List<RegionAlertInfo.State>): List<RegionAlertInfo.State> {
        val filteredStates = ArrayList<RegionAlertInfo.State>()
        states.forEach { state ->
            if (state.basicStatusInfo.regionName?.lowercase()?.contains(text) == true) {
                filteredStates.add(state.copy(districts = ArrayList()))
            }
            state.districts.forEach { district ->
                if (district.basicStatusInfo.regionName?.lowercase()?.contains(text) == true) {
                    val filteredState =
                        filteredStates.firstOrNull { it.basicStatusInfo.regionId == state.basicStatusInfo.regionId }
                            ?: run {
                                val clearedState = state.copy(districts = ArrayList())
                                filteredStates.add(clearedState)
                                clearedState
                            }
                    val clearedDistrict = district.copy(communities = ArrayList())
                    filteredState.districts.add(clearedDistrict)
                }
                district.communities.forEach { community ->
                    if (community.basicStatusInfo.regionName?.lowercase()?.contains(text) == true) {
                        val filteredState =
                            filteredStates.firstOrNull { it.basicStatusInfo.regionId == state.basicStatusInfo.regionId }
                                ?: run {
                                    val clearedState = state.copy(districts = ArrayList())
                                    filteredStates.add(clearedState)
                                    clearedState
                                }
                        val filteredDistrict =
                            filteredState.districts.firstOrNull { it.basicStatusInfo.regionId == district.basicStatusInfo.regionId }
                                ?: run {
                                    val clearedDistrict = district.copy(communities = ArrayList())
                                    filteredState.districts.add(clearedDistrict)
                                    clearedDistrict
                                }
                        filteredDistrict.communities.add(community)
                    }
                }
            }
        }
        return filteredStates
    }
}