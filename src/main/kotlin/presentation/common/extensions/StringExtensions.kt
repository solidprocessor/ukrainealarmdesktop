package presentation.common.extensions

import java.util.*

fun String.toSearchText(): String? = lowercase(Locale.getDefault()).trim().let {
    it.ifBlank {
        null
    }
}