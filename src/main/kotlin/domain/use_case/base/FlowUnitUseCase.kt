package domain.use_case.base

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

abstract class FlowUnitUseCase<Data> {
    open operator fun invoke() = execute().flowOn(Dispatchers.IO)

    protected abstract fun execute(): Flow<Result<Data>>
}