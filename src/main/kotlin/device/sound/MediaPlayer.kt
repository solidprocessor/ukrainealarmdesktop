package device.sound


class MediaPlayer {
    private val alertSound: PlayableSound = PlayableSound.AlertSound
    private val cancelAlertSound: PlayableSound = PlayableSound.AlertCancelSound

    private var playingSound: PlayableSound? = null

    private fun play(sound: PlayableSound) {
        try {
            with(sound.clip) {
                if (playingSound != sound || framePosition == frameLength) {
                    stopPlaying()
                    playingSound = sound
                    framePosition = 0
                    longFramePosition
                    start()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun playAlert() {
        play(alertSound)
    }

    fun playAlertCancel() {
        play(cancelAlertSound)
    }

    fun stopPlaying() {
        playingSound?.clip?.stop()
        playingSound = null
    }
}