package device.sound

import javax.sound.sampled.AudioSystem
import javax.sound.sampled.Clip

sealed class PlayableSound(resourcePath: String) {
    val clip: Clip = AudioSystem.getClip().loadFromResource(resourcePath)

    object AlertSound : PlayableSound("alarm1.wav")

    object AlertCancelSound : PlayableSound("sirene_off.wav")
}