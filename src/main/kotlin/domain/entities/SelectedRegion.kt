package domain.entities

data class SelectedRegion(val regionId: String, val regionName: String) : java.io.Serializable