package presentation.ui.regions

import domain.entities.RegionAlertInfo
import domain.entities.SelectedRegion
import domain.use_case.GetSelectedRegionUseCase
import domain.use_case.SaveSelectedRegionUseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import presentation.common.controller.ControllerState
import presentation.common.extensions.toControllerState
import presentation.common.extensions.toSelected


class SavedRegionController(
    private val getSelectedRegionUseCase: GetSelectedRegionUseCase,
    private val saveSelectedRegionUseCase: SaveSelectedRegionUseCase,
) {

    val state = MutableStateFlow<ControllerState<SelectedRegion?>>(ControllerState.Loading())

    fun refresh() {
        GlobalScope.launch {
            state.value = getSelectedRegionUseCase().toControllerState()
        }
    }

    fun save(region: RegionAlertInfo): SelectedRegion = region.toSelected().also { region ->
        GlobalScope.launch {
            saveSelectedRegionUseCase(region)
        }
    }
}