package presentation.common.extensions

import domain.entities.RegionAlertInfo
import domain.entities.SelectedRegion

fun RegionAlertInfo.getName(defaultText: String? = null): String =
    basicStatusInfo.regionName ?: defaultText ?: "Регіон ${basicStatusInfo.regionId}"

fun RegionAlertInfo.toSelected() = SelectedRegion(basicStatusInfo.regionId, getName())