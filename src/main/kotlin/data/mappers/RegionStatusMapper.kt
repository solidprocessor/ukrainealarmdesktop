package data.mappers

import data.models.RetrofitStatusObject
import domain.entities.RegionAlertInfo
import domain.entities.RegionAlertInfo.*
import domain.entities.RegionType

object RegionStatusMapper : OneDirectionMapper<List<RetrofitStatusObject>, RegionAlertInfo?> {

    override fun map(model: List<RetrofitStatusObject>): RegionAlertInfo? =
        model.firstOrNull()?.let { statusObject ->
            BasicStatusMapper.map(statusObject)?.let { basicInfo ->
                when (RegionTypeParser.parse(statusObject.regionType)) {
                    RegionType.STATE -> State(basicInfo)
                    RegionType.DISTRICT -> District(basicInfo)
                    RegionType.COMMUNITY -> Community(basicInfo)
                    else -> Unknown(basicInfo)
                }
            }
        }
}