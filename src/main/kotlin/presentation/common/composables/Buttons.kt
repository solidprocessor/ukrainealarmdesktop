package presentation.common.composables

import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import presentation.ui.constants.Colors

object Buttons {
    @Composable
    fun redButton(text: String, onClick: () -> Unit) {
        Button(onClick = onClick,
            colors = ButtonDefaults.buttonColors(backgroundColor = Colors.RED_BUTTON_BACKGROUND)) {
            Text(text, color = Color.White)
        }
    }
}