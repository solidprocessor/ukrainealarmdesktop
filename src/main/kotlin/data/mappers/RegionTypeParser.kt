package data.mappers

import domain.entities.RegionType

object RegionTypeParser {
    fun parse(text: String?) =
        when (text) {
            "State" -> RegionType.STATE
            "District" -> RegionType.DISTRICT
            "Community" -> RegionType.COMMUNITY
            else -> RegionType.UNKNOWN
        }
}