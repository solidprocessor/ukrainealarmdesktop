package presentation.common.themes

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import presentation.ui.constants.Colors

object Dark {
    @Composable
    fun colors() = MaterialTheme.colors.copy(
        primary = Colors.SELECTABLE_REGION_TEXT_COLOR,
        background = Colors.BACKGROUND_GRAY,
        surface = Colors.BACKGROUND_GRAY
    )
}