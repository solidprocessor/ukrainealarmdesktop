package data.models

data class RetrofitStatusObject(
    val regionId: String? = null,
    val regionName: String? = null,
    val lastUpdate: String? = null,
    val regionType: String? = null,
    val activeAlerts: List<RetrofitActiveAlert>? = emptyList()
)