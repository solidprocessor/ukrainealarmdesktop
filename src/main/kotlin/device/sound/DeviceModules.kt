package device.sound

import org.koin.dsl.module

object DeviceModules {
    val SOUND = module {
        single { MediaPlayer() }
    }

    val ALL = SOUND
}