package data.retrofit

import data.logger.LoggingInterceptor
import okhttp3.OkHttpClient
import java.time.Duration

object OkhttpClientBuilder {
    private const val TIMEOUT_SECONDS = 30L

    fun build(): OkHttpClient =
        Duration.ofSeconds(TIMEOUT_SECONDS).let { duration ->
            OkHttpClient.Builder()
                .callTimeout(duration)
                .connectTimeout(duration)
                .readTimeout(duration)
                .writeTimeout(duration)
                .addInterceptor(AuthInterceptor())
                .addInterceptor(LoggingInterceptor.build(headerToRedact = AuthInterceptor.HEADER))
                .build()
        }
}