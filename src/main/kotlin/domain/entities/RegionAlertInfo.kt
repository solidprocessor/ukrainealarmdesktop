package domain.entities

sealed interface RegionAlertInfo {
    val basicStatusInfo: BasicStatusInfo

    data class State(
        override val basicStatusInfo: BasicStatusInfo,
        val districts: ArrayList<District> = ArrayList()
    ) : RegionAlertInfo

    data class District(
        override val basicStatusInfo: BasicStatusInfo,
        var communities: ArrayList<Community> = ArrayList()
    ) : RegionAlertInfo

    data class Community(override val basicStatusInfo: BasicStatusInfo) : RegionAlertInfo
    data class Unknown(override val basicStatusInfo: BasicStatusInfo) : RegionAlertInfo
}