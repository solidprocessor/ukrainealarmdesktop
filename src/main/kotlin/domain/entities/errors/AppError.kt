package domain.entities.errors

sealed interface AppError {
    object NoInternetError : AppError {
        override fun toString(): String = "Немає інтернет-з'єднання"
    }

    class UnhandledError(val cause: Throwable, var message: String? = null) : AppError {
        override fun toString(): String =
            StringBuilder().run {
                if (message != null) {
                    append("$message:\n")
                }
                append(cause.stackTraceToString())
            }.toString()
    }
}