package domain.entities.errors

class CannotAccessGoogleException : IllegalStateException("Google server is not accessible")