package presentation.ui.regions

import domain.entities.RegionAlertInfo
import domain.use_case.GetRegionsUseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import presentation.common.controller.Controller
import presentation.common.controller.ControllerState
import presentation.common.extensions.toControllerState

class RegionsController(private val getRegionsUseCase: GetRegionsUseCase) : Controller {
    val state = MutableStateFlow<ControllerState<List<RegionAlertInfo.State>>>(ControllerState.Loading())

    private var statusJob: Job? = null

    override fun refresh() {
        statusJob = GlobalScope.launch {
            getRegionsUseCase().collect {
                state.emit(it.toControllerState())
            }
        }
    }

    override fun cancel() {
        statusJob?.cancel()
    }
}