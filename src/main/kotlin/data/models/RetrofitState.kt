package data.models

import com.google.gson.annotations.SerializedName

data class RetrofitState(
    val regionId: String? = null,
    val regionName: String? = null,
    val regionType: String? = null,
    @SerializedName("regionChildIds")
    val districts: List<RetrofitDistrict>? = null
)