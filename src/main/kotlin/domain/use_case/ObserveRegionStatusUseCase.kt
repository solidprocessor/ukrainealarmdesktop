package domain.use_case

import device.sound.MediaPlayer
import domain.entities.RegionAlertInfo
import domain.repositories.AlertsRepository
import domain.rules.AlertConstants
import domain.use_case.base.FlowUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*

class ObserveRegionStatusUseCase(private val repository: AlertsRepository, private val mediaPlayer: MediaPlayer) :
    FlowUseCase<String, RegionAlertInfo>() {
    override fun execute(params: String): Flow<Result<RegionAlertInfo>> {
        val timer = flow {
            while (true) {
                emit(Unit)
                delay(AlertConstants.REFRESH_PERIOD_SECONDS * 1000L)
            }
        }
        var previousInfo: RegionAlertInfo? = null
        val periodicRequest: Flow<Result<RegionAlertInfo>> = timer.flatMapLatest {
            repository.getStatusOfRegion(params)
                .flowOn(Dispatchers.IO)
        }
        val soundPlayer = periodicRequest.onEach { result ->
            if (result.isSuccess) {
                val newInfo = result.getOrThrow()
                if (previousInfo == null && newInfo.basicStatusInfo.isAlertActive()) {
                    mediaPlayer.playAlert()
                }
                previousInfo?.let {
                    val alertWasInProgress = it.basicStatusInfo.isAlertActive()
                    val alertIsInProgress = newInfo.basicStatusInfo.isAlertActive()
                    if (alertWasInProgress != alertIsInProgress) {
                        if (alertIsInProgress) {
                            mediaPlayer.playAlert()
                        } else {
                            mediaPlayer.playAlertCancel()
                        }
                    }
                }
                previousInfo = newInfo
            }
        }
        return soundPlayer
    }

}