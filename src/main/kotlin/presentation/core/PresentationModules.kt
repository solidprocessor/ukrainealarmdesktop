package presentation.core

import device.sound.MediaPlayer
import org.koin.dsl.module
import presentation.ui.alert.AlertComposable
import presentation.ui.alert.RegionStatusController
import presentation.ui.regions.RegionsComposable
import presentation.ui.regions.RegionsController
import presentation.ui.regions.SavedRegionController

object PresentationModules {
    val CONTROLLERS = module {
        single { RegionsController(get()) }
        single { SavedRegionController(get(), get()) }
        single { RegionStatusController(get(), get()) }
        single { MediaPlayer() }
    }
    val UI = module {
        single { RegionsComposable(get(), get()) }
        single { AlertComposable(get()) }
    }

    val ALL = CONTROLLERS + UI
}