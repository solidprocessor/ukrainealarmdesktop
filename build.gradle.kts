import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.io.FileInputStream
import java.util.*

plugins {
    kotlin("jvm") version "1.6.10"
    id("org.jetbrains.compose") version "1.1.1"
    id("com.github.gmazzo.buildconfig") version "3.0.3"
}

group = "com.ukraine"
version = "1.0"

repositories {
    google()
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
}

dependencies {
    implementation(compose.desktop.currentOs)
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}

compose.desktop {
    application {
        mainClass = "MainKt"
        nativeDistributions {
            modules("java.sql")
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "UkraineAlert"
            packageVersion = "1.0.0"
            description = "Ukraine alert application"
            vendor = "Vsevolod Salenko"
            val iconPath = project.file("src/main/resources/ic_launcher.png")
            macOS {
                iconFile.set(iconPath)
            }
            windows {
                iconFile.set(iconPath)
            }
            linux {
                iconFile.set(iconPath)
            }
        }
    }
}

val backendProperties =
    try {
        Properties().apply {
            load(FileInputStream(File(rootProject.rootDir, "alerts_backend.properties")))
        }
    } catch (error: Exception) {
        null
    }

buildConfig {
    val defaultValue = "TEST"

    buildConfigField(
        type = "String",
        name = "ALERTS_BASE_URL",
        value = "\"${backendProperties?.getProperty("ALERTS_BASE_URL") ?: defaultValue}\""
    )
    buildConfigField(
        type = "String",
        name = "ALERTS_TOKEN",
        value = "\"${backendProperties?.getProperty("ALERTS_TOKEN") ?: defaultValue}\""
    )
    buildConfigField(
        type = "Boolean",
        name = "TEST_MODE",
        value = "${backendProperties == null}"
    )
}

dependencies {
//Retrofit:
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")
    implementation("com.squareup.okhttp3:logging-interceptor:4.10.0")
//DI:
    implementation("io.insert-koin:koin-core:3.2.0")
//Reactive
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.2")

}