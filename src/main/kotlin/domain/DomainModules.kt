package domain

import domain.use_case.*
import org.koin.dsl.module

object DomainModules {
    //Requires AlertsRepository implementation
    val USE_CASE = module {
        factory { GetRegionsUseCase(get()) }
        factory { GetRegionStatusUseCase(get()) }
        factory { GetActiveAlertsUseCase(get()) }
        factory { ObserveRegionStatusUseCase(get(), get()) }
        factory { SaveSelectedRegionUseCase(get()) }
        factory { GetSelectedRegionUseCase(get()) }
    }

    val ALL = USE_CASE
}