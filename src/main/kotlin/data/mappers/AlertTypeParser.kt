package data.mappers

import domain.entities.AlertType

object AlertTypeParser {
    fun parse(text: String?): AlertType? =
        when (text) {
            "Null" -> AlertType.NONE
            "Air" -> AlertType.AIR
            "StreetFight" -> AlertType.STREET_FIGHT
            else -> null
        }
}