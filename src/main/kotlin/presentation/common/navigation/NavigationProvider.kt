package presentation.common.navigation

import androidx.compose.runtime.Composable
import domain.entities.RegionAlertInfo
import domain.entities.SelectedRegion
import presentation.ui.alert.AlertComposable
import presentation.ui.regions.RegionsComposable

class NavigationProvider(
    private val regionsScreen: RegionsComposable,
    private val alertScreen: AlertComposable,
) {
    @Composable
    fun navigate(navigationDirection: NavigationDirection) {
        when (navigationDirection) {
            is NavigationDirection.AlertScreen -> alertScreen(navigationDirection.selectedRegion)
            is NavigationDirection.RegionsScreen -> regionsScreen(navigationDirection.selectedRegion)
            NavigationDirection.Pending -> Unit
        }
    }

    @Composable
    private fun regionsScreen(selectedRegion: RegionAlertInfo? = null) = regionsScreen.compose(selectedRegion)

    @Composable
    private fun alertScreen(selectedRegion: SelectedRegion) = alertScreen.compose(selectedRegion)
}