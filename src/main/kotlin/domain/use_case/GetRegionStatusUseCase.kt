package domain.use_case

import domain.entities.RegionAlertInfo
import domain.repositories.AlertsRepository
import domain.use_case.base.FlowUseCase
import kotlinx.coroutines.flow.Flow

class GetRegionStatusUseCase(private val alertsRepository: AlertsRepository) : FlowUseCase<String, RegionAlertInfo>() {
    override fun execute(params: String): Flow<Result<RegionAlertInfo>> = alertsRepository.getStatusOfRegion(params)
}