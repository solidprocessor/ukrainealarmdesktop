package domain.entities

import com.google.gson.annotations.SerializedName

enum class AlertType {
    @SerializedName("Null")
    NONE,

    @SerializedName("Air")
    AIR,

    @SerializedName("StreetFight")
    STREET_FIGHT
}