package data.models

data class RetrofitCommunity(
    val regionId: String? = null,
    val regionName: String? = null,
    val regionType: String? = null
)