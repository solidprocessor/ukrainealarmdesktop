package presentation.common.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import presentation.ui.constants.Colors.TEXT_COLOR

object Texts {
    @Composable
    fun clickableText(
        text: String,
        textColor: Color = TEXT_COLOR,
        textSize: TextUnit = TextUnit.Unspecified,
        background: Color = Color.Transparent,
        paddingStart: Dp = 0.dp,
        paddingTop: Dp = 0.dp,
        paddingEnd: Dp = 0.dp,
        paddingBottom: Dp = 0.dp,
        decoration: TextDecoration? = null,
        clickListener: (Int) -> Unit,
    ) {
        ClickableText(
            text = AnnotatedString(text),
            modifier = Modifier.fillMaxWidth().background(background)
                .padding(paddingStart, paddingTop, paddingEnd, paddingBottom),
            style = TextStyle(color = textColor, fontSize = textSize, textDecoration = decoration),
            onClick = clickListener
        )
    }
}