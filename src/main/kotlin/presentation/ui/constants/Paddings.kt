package presentation.ui.constants

import androidx.compose.ui.unit.dp

object Paddings {
    val REGION_PADDING = 64.dp
}