package data.models

data class RetrofitActiveAlert(
    val regionId: String? = null,
    val regionType: String? = null,
    val type: String? = null,
    val lastUpdate: String = ""
)