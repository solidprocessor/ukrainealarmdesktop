package domain.rules

object AlertConstants {
    const val REFRESH_PERIOD_SECONDS = 15
}