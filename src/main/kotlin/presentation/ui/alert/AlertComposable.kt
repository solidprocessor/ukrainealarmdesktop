package presentation.ui.alert

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import domain.entities.RegionAlertInfo
import domain.entities.SelectedRegion
import presentation.common.composables.Modifiers
import presentation.common.controller.ControllerState
import presentation.common.navigation.Navigation
import presentation.common.navigation.NavigationDirection
import presentation.common.themes.Dark
import presentation.ui.constants.Colors
import presentation.ui.constants.Fonts

class AlertComposable(private val regionStatusController: RegionStatusController) {

    @Composable
    fun compose(selectedRegion: SelectedRegion) {
        val regionState by regionStatusController.status.collectAsState()
        regionStatusController.startObserving(selectedRegion.regionId)
        MaterialTheme(colors = Dark.colors()) {
            Column(modifier = Modifiers.fillWithBorder(), verticalArrangement = Arrangement.SpaceBetween) {
                title(selectedRegion.regionName, regionState)
                alertStatus(regionState)
                bottomButtons()
            }
        }
    }

    @Composable
    private fun title(text: String, state: ControllerState<RegionAlertInfo>) {
        val color = when (state) {
            is ControllerState.Data ->
                if (state.data.basicStatusInfo.isAlertActive()) {
                    Colors.TEXT_COLOR_ALERT
                } else {
                    Colors.TEXT_COLOR
                }
            is ControllerState.Error -> Colors.TEXT_COLOR_WARNING
            is ControllerState.Loading -> Colors.TEXT_COLOR
        }
        Column(
            verticalArrangement = Arrangement.Top,
            modifier = Modifier.fillMaxWidth().clickable { goToTheRegionsScreen() }
        ) {
            Row {
                Text(
                    text = text,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth(),
                    color = color,
                    fontSize = Fonts.Sizes.ALERT_TITLE
                )
            }
            Text(
                text = "Змінити",
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth().padding(bottom = 18.dp),
                color = Colors.SELECTABLE_REGION_TEXT_COLOR
            )
            Divider(color = color)

        }
    }

    @Composable
    private fun alertStatus(status: ControllerState<RegionAlertInfo>) {
        val textAndColor = when (status) {
            is ControllerState.Data ->
                if (status.data.basicStatusInfo.isAlertActive()) {
                    Pair("!!!ТРИВОГА, В УКРИТТЯ!!!", Colors.TEXT_COLOR_ALERT)
                } else {
                    Pair("Немає тривоги", Colors.TEXT_COLOR)
                }
            is ControllerState.Error -> Pair("Помилка. Спроба встановити з'єднання...", Colors.TEXT_COLOR_WARNING)
            is ControllerState.Loading -> Pair("Завантаження...", Colors.TEXT_COLOR)
        }
        Text(text = textAndColor.first,
            color = textAndColor.second,
            fontSize = 48.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth().wrapContentHeight(align = Alignment.CenterVertically, unbounded = true)
        )
    }

    @Composable
    private fun bottomButtons() {
        Column(Modifier.fillMaxWidth(), verticalArrangement = Arrangement.Bottom) {
            Row { Text("Ручне керування звуком", color = Colors.TEXT_COLOR) }
            Divider(color = Colors.TEXT_COLOR)
            Row(modifier = Modifier.fillMaxWidth().padding(top = 18.dp),
                horizontalArrangement = Arrangement.SpaceAround) {
                Column {
                    Button(colors = ButtonDefaults.buttonColors(Colors.ERROR_BACKGROUND),
                        onClick = { regionStatusController.playAlertSound() }) {
                        Text("Тривога", color = Colors.TEXT_COLOR)
                    }
                }
                Column {
                    Button(
                        colors = ButtonDefaults.buttonColors(Color.White),
                        onClick = { regionStatusController.playAlertCancelSound() }
                    ) {
                        Text("Відбій тривоги", color = Color.Black)
                    }
                }
                Column {
                    Button(
                        onClick = { regionStatusController.stopPlaying() },
                        colors = ButtonDefaults.buttonColors(Color.White)
                    ) {
                        Text("Припинити", color = Color.Black)
                    }
                }
            }
        }
    }

    private fun goToTheRegionsScreen() {
        Navigation.currentScreen.value = NavigationDirection.RegionsScreen()
        regionStatusController.stopObserving()
        regionStatusController.stopPlaying()
    }

}