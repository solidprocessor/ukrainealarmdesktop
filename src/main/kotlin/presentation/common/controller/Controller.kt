package presentation.common.controller

interface Controller {
    fun refresh()
    fun cancel()
}