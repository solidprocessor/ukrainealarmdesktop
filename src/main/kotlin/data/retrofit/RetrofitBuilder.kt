package data.retrofit

import com.ukraine.UkraineAlertCompose.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    fun build(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.ALERTS_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}