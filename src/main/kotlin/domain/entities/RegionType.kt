package domain.entities

enum class RegionType {
    STATE, DISTRICT, COMMUNITY, UNKNOWN
}