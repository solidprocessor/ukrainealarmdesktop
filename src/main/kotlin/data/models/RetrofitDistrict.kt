package data.models

import com.google.gson.annotations.SerializedName

data class RetrofitDistrict(
    val regionId: String? = null,
    val regionName: String? = null,
    val regionType: String? = null,
    @SerializedName("regionChildIds")
    val communities: List<RetrofitCommunity>? = null
)