package domain.use_case

import domain.entities.SelectedRegion
import domain.repositories.SelectedRegionRepository

class SaveSelectedRegionUseCase(private val selectedRegionRepository: SelectedRegionRepository) {
    suspend operator fun invoke(selectedRegion: SelectedRegion) = selectedRegionRepository.save(selectedRegion)
}