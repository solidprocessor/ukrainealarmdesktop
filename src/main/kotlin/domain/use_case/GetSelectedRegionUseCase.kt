package domain.use_case

import domain.repositories.SelectedRegionRepository

class GetSelectedRegionUseCase(private val selectedRegionRepository: SelectedRegionRepository) {
    suspend operator fun invoke() = selectedRegionRepository.get()
}