package data.retrofit

import com.ukraine.UkraineAlertCompose.BuildConfig
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthInterceptor : Interceptor {
    companion object {
        const val HEADER = "Authorization"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest: Request = chain.request().newBuilder()
            .addHeader(HEADER, BuildConfig.ALERTS_TOKEN)
            .build()
        return chain.proceed(newRequest)
    }
}