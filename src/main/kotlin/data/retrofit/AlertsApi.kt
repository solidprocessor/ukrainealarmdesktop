package data.retrofit

import data.models.RetrofitAlertInfo
import data.models.RetrofitRegionsResponse
import data.models.RetrofitStatusObject
import retrofit2.http.GET
import retrofit2.http.Path

interface AlertsApi {
    @GET("Regions")
    suspend fun getRegions(): RetrofitRegionsResponse

    @GET("alerts/{regionId}")
    suspend fun getStatusOfRegion(@Path("regionId") regionId: String): List<RetrofitStatusObject>

    @GET("alerts")
    @Deprecated("response model is updated")
    suspend fun getAlerts(): List<RetrofitAlertInfo>
}