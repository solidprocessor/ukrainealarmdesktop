package domain.logs

interface Logger {
    fun print(message: String)
    fun println(message: String)
}